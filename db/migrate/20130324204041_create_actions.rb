class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.integer :ticket_id
      t.string :description
      t.integer :technician_id

      t.timestamps
    end
  end
end
