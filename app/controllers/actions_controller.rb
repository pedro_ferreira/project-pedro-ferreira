class ActionsController < ApplicationController
  # GET /actions
  # GET /actions.json
  before_filter :get_ticket

  def index
    @actions = @ticket.actions

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @actions }
    end
  end

  # GET /actions/1
  # GET /actions/1.json
  def show
    @action = @ticket.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ticket }
    end
  end

  # GET /actions/new
  # GET /actions/new.json
  def new
    @ticket = Ticket.find(params[:ticket_id])
    @action = @ticket.actions.build
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @action }
    end
  end

  # GET /actions/1/edit
  def edit
    @action = @ticket.actions.find(params[:id])
  end

  # POST /actions
  # POST /actions.json
  def create
    @action = @ticket.actions.build(params[:action])

    respond_to do |format|
      if @action.save
        format.html { redirect_to ticket_actions_url(@ticket), notice: 'Action was successfully created.' }
        format.json { render json: @action, status: :created, location: @action }
      else
        format.html { render action: "new" }
        format.json { render json: @action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /actions/1
  # PUT /actions/1.json
  def update
    @action = @ticket.actions.find(params[:id])

    respond_to do |format|
      if @action.update_attributes(params[:action])
        format.html { redirect_to ticket_action_url(@action), notice: 'Action was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actions/1
  # DELETE /actions/1.json
  def destroy
    @action = @ticket.actions.find(params[:id])
    @action.destroy

    respond_to do |format|
      format.html { redirect_to (ticket_actions_path(@ticket)) }
      format.json { head :no_content }
    end
  end
  def get_ticket
    @ticket = Ticket.find(params[:ticket_id])
  end
end
