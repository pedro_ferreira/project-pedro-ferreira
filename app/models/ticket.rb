class Ticket < ActiveRecord::Base
  attr_accessible :customer_id, :problem
  belongs_to :customer
  has_many :actions

  #require data in fields
  validates_presence_of :problem, :customer_id

  # @return [Search]
  def self.search search_term
     return scoped unless search_term.present?
     where(['customers: ', "%#{search_term}%"])
   end

end

