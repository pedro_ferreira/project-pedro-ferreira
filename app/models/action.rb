class Action < ActiveRecord::Base
  attr_accessible :description, :technician_id, :ticket_id
  belongs_to :ticket
  belongs_to :user

  #require data in fields
  validates_presence_of :description, :technician_id, :ticket_id
end
