class Technician < ActiveRecord::Base
  attr_accessible :name
  #require data in fields
  validates_presence_of :name
end
