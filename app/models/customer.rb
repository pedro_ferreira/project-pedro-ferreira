class Customer < ActiveRecord::Base
  attr_accessible :contact, :name
  has_many :tickets
  #require data in fields
  validates_presence_of :contact, :name
end
